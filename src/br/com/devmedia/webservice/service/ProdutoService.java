package br.com.devmedia.webservice.service;

import java.util.List;

import br.com.devmedia.webservice.model.dao.ProdutoDAO;
import br.com.devmedia.webservice.model.domain.Produto;

public class ProdutoService {

	private ProdutoDAO dao = new ProdutoDAO();

	public List<Produto> getProdutos(long marcaId) {
		return dao.getAll(marcaId);
	}

	public Produto getProduto(Long id) {
		return dao.getById(id);
	}
	
	public List<Produto> getProdutosByPagination(
		long marcaId, int firstResult, int maxResults) {
		return dao.getByPagination(marcaId, firstResult, maxResults);
	}

	public List<Produto> getProdutoByName(long marcaId, String name) {
		return dao.getByName(marcaId, name);
	}

	public Produto saveProduto(long marcaId, Produto produto) {
		return dao.save(marcaId, produto);
	}

	public Produto updateProduto(long marcaId, Produto produto) {
		return dao.update(marcaId, produto);
	}

	public Produto deleteProduto(Long id) {
		return dao.delete(id);
	}

}